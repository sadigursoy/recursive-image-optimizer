# Recursive Image Optimizer

Recursively scan folders and optimize images on your server. 

The optimizations log and the sizes of your files before/after compression is logged in a csv file.


## Caution

This file optimizes the image **in-place**. Your original image will be replaced with the optimized one. 

So in case you want to undo this operation, please have your images backed-up before running this script.



### Prerequisites

This project uses [Composer](https://getcomposer.org/) for dependency management.  If you don't have composer installed, please install it first.


### Installing

After composer is installed, run  **composer install** to install dependencies.

```
composer install
```


### Edit src/Optimize.php file for your environment

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Set **setSrcPath** as the path for your images. *(Full path should be given)*
2. Set **setCsvPath** as the csv file for the transaction log. *(Full path should be given)*
3. Set **setFilesIncluded** as array of file types that should be processed. 
4. Set **setFilesExcluded** as array of file types that should *NOT* be processed. 
5. Set **setJpgCompression** as JPG file compression ratio. 

## Run script

You can run the script by simply the command below:

```php src/Optimize.php```


The script will recursively search all folders in your directory and optimize all the images. 
The names of the processed images will be written the console, and also to the CSV file you specified.



### Built With

* [ps/image-optimizer](https://packagist.org/packages/ps/image-optimizer) - Image optimization / compression library
* [theseer/DirectoryScanner](https://github.com/theseer/DirectoryScanner) - PHP File Scanner
* [sebastiansulinski/file](https://packagist.org/packages/sebastiansulinski/file) - File information package

### Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

### Authors

* **Sadi Gursoy** - [BitBucket](https://bitbucket.org/sadigursoy)

### License

This project is licensed under the MIT License
