<?php
/**
 * Created by PhpStorm.
 *
 * Batch image optimization package.
 *
 * User: sadigursoy
 * Date: 11.02.2019
 * Time: 10:28
 */

namespace MGA;

require __DIR__ . '/../vendor/autoload.php';

$optimizer = new \MGA\Optimizer();


$optimizer->setSrcPath('/recursive-optimizer-test');
$optimizer->setCsvPath('/recursive-optimizer-test/test.csv');
$optimizer->setFilesIncluded([
    '*.jp*g', // jpg, jpeg
    '*.png',
]);
$optimizer->setFilesExcluded([]);

$optimizer->setJpgCompression(80);

$optimizer->process();
