<?php
/**
 * Created by PhpStorm.
 * User: sadigursoy
 * Date: 11.02.2019
 * Time: 10:25
 */

namespace MGA;

use SSD\File\File;
use SSD\File\Type\Image;

class Optimizer
{

    /**
     * Source folder for images
     *
     * @var string
     */
    private $srcPath = '';

    /**
     * The output file. Full path should be given.
     *
     * @var string
     */
    private $csvPath = '';


    /**
     * Hangi dosyalarda işlem yapılacağı
     * @var array
     */
    private $filesIncluded = [];

    /**
     * Files to be excluded
     *
     * @var array
     */
    private $filesExcluded = [];

    /**
     * JPG compression ratio
     * @var string
     */
    private $jpgCompression = null;


    private $scanner; //  \TheSeer\DirectoryScanner\DirectoryScanner

    private $processedFiles = [];

    private $startDateTime = null;

    private $finishDateTime = null;

    /**
     * @param string $srcPath
     */
    public function setSrcPath($srcPath)
    {
        $this->srcPath = $srcPath;
    }

    /**
     * @param string $csvPath
     */
    public function setCsvPath($csvPath)
    {
        $this->csvPath = $csvPath;
    }

    /**
     * @param array $filesIncluded
     */
    public function setFilesIncluded($filesIncluded)
    {
        $this->filesIncluded = $filesIncluded;
    }

    /**
     * @param array $filesExcluded
     */
    public function setFilesExcluded($filesExcluded)
    {
        $this->filesExcluded = $filesExcluded;
    }

    /**
     * @param string $jpgCompression
     */
    public function setJpgCompression($jpgCompression)
    {
        $this->jpgCompression = $jpgCompression;
    }

    public function __construct()
    {
        $this->scanner = new \TheSeer\DirectoryScanner\DirectoryScanner;
    }


    public function process()
    {
        $factory = new \ImageOptimizer\OptimizerFactory();
        $optimizer = $factory->get();

        foreach ($this->filesIncluded as $included) {
            $this->scanner->addInclude($included);
        }

        foreach ($this->filesExcluded as $excluded) {
            $this->scanner->addExclude($excluded);
        }

        $this->startDateTime = date("d.m.Y H:i:s");
        echo 'Started: ' . $this->startDateTime . PHP_EOL;

        $scanner = $this->scanner;

        $cnt = 0;

        foreach($scanner($this->srcPath) as $fileRef) {
            $cnt++;

            $path = $fileRef->getPathname();

            echo $path . PHP_EOL;

            $file = new File($path);

            try{
                $image = new Image($file);

                array_push($this->processedFiles, array(
                        'path' => $path,
                        'type' => $image->extension(),
                        'pre' => $image->sizeInBytes(),
                        'post' => null)
                );

                $optimizer->optimize($path);

                echo $cnt % 100 == 0 ? 'Processed ' . $cnt . ' files..' . PHP_EOL : '';
            }catch (\Exception $e){
                echo '**** ERROR START ****' . PHP_EOL;
                echo $path . PHP_EOL;
                echo $e->getMessage() . PHP_EOL;
                echo '**** ERROR END ****' . PHP_EOL;
            }
        }

        $this->finishDateTime = date("d.m.Y H:i:s");
        $this->saveCSV();

        echo 'Total ' . count($this->processedFiles) . ' files..' . PHP_EOL;
        echo 'Finished: ' . $this->finishDateTime . PHP_EOL;
    }

    private function saveCSV(){
        $fp = fopen($this->csvPath, 'w');
        fputcsv($fp, array('Filename', 'Extension',  'Size Before', 'Size After'));

        $totalPre = 0;
        $totalPost = 0;

        foreach ($this->processedFiles as &$file){
            $image = new Image(new File($file['path']));
            $file['post'] = $image->sizeInBytes();

            $totalPre += $file['pre'];
            $totalPost += $file['post'];

            fputcsv($fp, $file);
        }

        fputcsv($fp, array('Total', '',  $totalPre, $totalPost));
        fputcsv($fp, array('Start', $this->startDateTime, '', ''));
        fputcsv($fp, array('Finish', $this->finishDateTime, '', ''));

        fclose($fp);
    }

}